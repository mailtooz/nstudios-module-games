<?php

namespace NStudios\Games\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class AllGames extends Template implements BlockInterface
{

    
    protected $_template = "NStudios_Games::widget/games.phtml";

}
