'use strict';

var cardsArray = [{
  'name': 'shell',
  'img': 'img/sqs.png'
}, {
  'name': 'star',
  'img': 'img/star.png'
}, {
  'name': 'bobomb',
  'img': 'img/bobomb.png'
}, {
  'name': 'mario',
  'img': 'img/mario.png'
}, {
  'name': 'luigi',
  'img': 'img/luigi.png'
},
 {
  'name': 'peach',
  'img': 'img/peach.png'
}, {
  'name': '1up',
  'img': 'img/1up.png'

}, {
  'name': 'mushroom',
  'img': 'img/mushroom.png'
}, {
  'name': 'thwomp',
  'img': 'img/thwomp.png'
}, {
  'name': 'bulletbill',
  'img': 'img/bulletbill.png'
}, {
  'name': 'coin',
  'img': 'img/coin.png'
}, {
  'name': 'goomba',
  'img': 'img/goomba.png'
}
];
var gameGrid = cardsArray.concat(cardsArray).sort(function () {
  return 0.5 - Math.random();
});

var firstGuess = '';
var secondGuess = '';
var count = 0;
var previousTarget = null;
var delay = 1200;
var win = 0;
var game = document.getElementById('game');
var grid = document.createElement('section');
grid.setAttribute('class', 'grid');
game.appendChild(grid);



function start(){

  gameGrid.forEach(function (item) {
  var name = item.name,
      img = item.img;


  var card = document.createElement('div');
  card.classList.add('card');
  card.dataset.name = name;

  var front = document.createElement('div');
  front.classList.add('front');

  var back = document.createElement('div');
  back.classList.add('back');
  back.style.backgroundImage = 'url(' + img + ')';

    grid.appendChild(card);
    card.appendChild(front);
    card.appendChild(back);
  });


}

var match = function match() {
  var selected = document.querySelectorAll('.selected');
  selected.forEach(function (card) {
    card.classList.add('match');
  });
};



var resetGuesses = function resetGuesses() {
  firstGuess = '';
  secondGuess = '';
  count = 0;
  previousTarget = null;

  var selected = document.querySelectorAll('.selected');
  selected.forEach(function (card) {
    card.classList.remove('selected');
  });
};

function modal(){
  var modal = document.getElementById('GameModal');
  
  // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
  // When the user clicks on the button, open the modal 
    modal.style.display = "block";
  
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
}

function newGame(){
  //close menu
  var modal = document.getElementById('GameModal');
  modal.style.display="none";

  var restart = document.querySelectorAll('.match');
   restart.forEach(function (card) {
     card.classList.remove('match');
   });
   
   var gameGrid = cardsArray.concat(cardsArray).sort(function () {
    return 0.5 - Math.random();
    });
  
    var back = document.querySelectorAll('.back');
    var card = document.querySelectorAll('.card');
 
    
     
      for (let i = 0; i < gameGrid.length; i++) {
        var name = gameGrid[i].name;
        var img = gameGrid[i].img;
        
        console.log(name);
        // console.log(img);
        
        card[i].dataset.name = name;
        back[i].style.backgroundImage = 'url(' + img + ')';
        
      }
   
  
}

grid.addEventListener('click', function (event) {

  var clicked = event.target;

  if (clicked.nodeName === 'SECTION' || clicked === previousTarget || clicked.parentNode.classList.contains('selected') || clicked.parentNode.classList.contains('match')) {
    return;
  }

  if (count < 2) {
    count++;
    if (count === 1) {
      firstGuess = clicked.parentNode.dataset.name;
      // console.log(firstGuess);
      clicked.parentNode.classList.add('selected');
    } else {
      
      secondGuess = clicked.parentNode.dataset.name;
      // console.log(secondGuess);
      clicked.parentNode.classList.add('selected');
    }

    if (firstGuess && secondGuess) {
      if (firstGuess === secondGuess) {
        winGame();
        setTimeout(match, delay);
        
      }
      setTimeout(resetGuesses, delay);
    }
    previousTarget = clicked;
  }
});

function winGame(){
  win ++
  if (win >= 2) {
      count = 0;
      win = 0;
      modal();
      
  }
}

start();

