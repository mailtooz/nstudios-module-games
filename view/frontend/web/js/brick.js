define(['jquery'], function ($, config) {
    $(document).ready(function () {
        $("body").addClass('playzone');
    });
    return function (config) {
        var brickWrapper = document.getElementById('brickWrapper');

        var img = document.createElement('img');
        img.src = config.options + '/hensan.png';

        var game = document.getElementById('squish-brick'),
            ctx = game.getContext("2d");
        //Settings
        var
            animation,
            // Ball Size
            ballRadius = 10,
            dx = 2,
            dy = -2,
            // Paddle Positioning & size 
            paddleW = game.width / 6,
            paddleH = 10,
            paddleY = game.height - paddleH,
            // paddleY = game.height-paddleH,
            paddleX = Math.floor((Math.random() * game.width / 2) + 1 + paddleW),
            // Breaks Sizes
            bricksW = game.width / 10,
            bricksH = game.height / 10,
            brickColumnCount = 7,
            brickRowCount = 3,
            brickPadding = 3,
            brickOffsetTop = (game.height / 2) / 6,
            brickOffsetLeft = game.width / 2 - 190;
        // Bricks Positioning
        var bricks = [];
        for (let c = 0; c < brickColumnCount; c++) {
            bricks[c] = [];

            for (let r = 0; r < brickRowCount; r++) {
                bricks[c][r] = { x: 0, y: 0, status: 1 };
            }
        }
        var x = game.width / 2,
            y = game.height- paddleH - ballRadius;
        // Game Settings
        var score = 0,
            lives = 3,
            fscore = 0;//fake score
        //ball speed
        var speed = 12;
        //Controller Settings
        var rightPressed = false,
            leftPressed = false,
            upPressed = false,
            downPressed = false;


        // Draw Ball
        function drawBall() {
            ctx.fillStyle = "#e74c3c";
            ctx.beginPath();
            ctx.arc(x, y, ballRadius, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();
        }
        function drawPaddle() {
            ctx.fillStyle = "#2c3e50";
            ctx.beginPath();
            ctx.rect(paddleX, paddleY, paddleW, paddleH);
            ctx.closePath();
            ctx.fill();
        }
        function drawbricks() {
            for (c = 0; c < brickColumnCount; c++) {
                for (r = 0; r < brickRowCount; r++) {
                    if (bricks[c][r].status == 1) {
                        var brickX = (c * (bricksW + brickPadding)) + brickOffsetLeft;
                        var brickY = (r * (bricksH + brickPadding)) + brickOffsetTop;
                        bricks[c][r].x = brickX;
                        bricks[c][r].y = brickY;
                        ctx.drawImage(img, brickX, brickY, bricksW, bricksH);

                    }
                }
            }
        }
        var keyup = document.getElementById("keyUp"),
            keydown = document.getElementById("keyDown"),
            keyright = document.getElementById("keyRight"),
            keyleft = document.getElementById("keyLeft");

        keyup.addEventListener('click', keyUp, false);
        keydown.addEventListener('click', keyDown, false);
        keyright.addEventListener('click', keyRight, false);
        keyleft.addEventListener('click', keyLeft, false);
        function keyUp() {
            if (paddleY > game.height / 2) {
                paddleY -= 20;
            }
        }
        function keyDown() {
            if (paddleY < game.height - paddleH) {
                paddleY += 20;
            }
        }
        function keyRight() {
            if (paddleX < game.width - paddleW) {
                paddleX += paddleW;
                if (paddleX > game.width - paddleW) {
                    paddleX = game.width - paddleW;
                }
            }
        }
        function keyLeft() {
            if (paddleX > 0) {
                paddleX -= paddleW;
                if (paddleX < 0) {
                    paddleX = 0;
                }
            }
        }

        function drawScore() {
            ctx.fillStyle = '#e74c3c';
            ctx.font = "16px Arial";
            ctx.fillText("Score: " + score, (game.width / 2) - 20, 19);
        }
        var level = 1;
        function drawLevel() {
            ctx.fillStyle = "#e67e22";
            ctx.font = "16px Arial";
            ctx.fillText('Level:' + level, 8, 19);
        }

        function drawLives() {
            ctx.font = "16px Arial";
            ctx.fillText("Lives: " + lives, game.width - 59, 19);
        }
        function collisionDetection() {
            for (c = 0; c < brickColumnCount; c++) {
                for (r = 0; r < brickRowCount; r++) {
                    var b = bricks[c][r];
                    if (b.status == 1) {
                        if (x - 10 > b.x && x - 10 < b.x + bricksW && y - 10 > b.y && y - 10 < b.y + bricksH) {
                            dy = -dy;
                            b.status = 0;
                            score++;
                            fscore++    
                            if (fscore == brickRowCount * brickColumnCount) {
                                if (level > 2) {
                                    restartBtn('Your score is!! ' + score, 'Would you like to play again?', 'Restart Game');
                                    clearInterval(animation);
                                } else {
                                    level++;
                                    levelUp();
                                }
                            }
                        }
                    }
                }
            }
        }

        var counterTime = 0;
        var counterDown = 3

        function levelUp() {
            clearInterval(animation);
            drawCounter();
            counterTime = 0;
            counterDown = 3
            var loop = setInterval(function () {
                x = game.width/ 2;
                // y = game.height - paddleH - ballRadius;
                y = game.height - paddleH - ballRadius - 10;
                console.log("Ball: "+ y);
                console.log("start point: "+ game.height);
                paddleX = game.width/2;
                paddleY = game.height  - paddleH;
                fscore = 0;
                // Bricks Positioning
                for (let c = 0; c < brickColumnCount; c++) {
                    bricks[c] = [];
                    for (let r = 0; r < brickRowCount; r++) {
                        bricks[c][r] = { x: 0, y: 0, status: 1 };
                    }
                }
                if (level === 2) {
                    speed = 10;
                } else if (level === 3) {
                    speed = 7;
                }
                clearInterval(loop);
                dx = 2;
                dy = -2;
                animation = setInterval(draw, speed);

            }, 3000)
        }


        function drawCounter() {
            var countDownBox = document.createElement('div');
            countDownBox.classList.add('count-down-box');
            var counDownText = document.createTextNode('');
            brickWrapper.appendChild(countDownBox);
            countDownBox.appendChild(counDownText);
            countDownBox.style.fontSize = "35";
            var counter = setInterval(function () {

                counterTime++;
                if (counterTime > 3) {
                    clearInterval(counter);
                    countDownBox.innerHTML = "";
                } else {

                    countDownBox.innerHTML = counterDown--;
                }
            }, 1000)
        }

        function draw() {
            ctx.clearRect(0, 0, game.width, game.height);
            drawBall();
            drawPaddle();
            drawbricks();
            collisionDetection();
            drawScore();
            drawLives();
            drawLevel();
            x += dx;
            y += dy;
            // KEEP BALL INSIDE THe FRAME
            //side walls
            if (x + dx > game.width - ballRadius || x + dx < ballRadius) {
                dx = -dx;
            }
            //up wall
            if (y + dy < ballRadius) {
                dy = -dy;
            }
            //hit the ball
            else if (y + dy === paddleY - ballRadius) {
                if (x > paddleX && x < paddleX + paddleW) {
                    dy = -dy;
                }
            }
            //game over
            if (y + dy === game.height - paddleH) {
                lives--;
                    if (lives <= 0) {
                        // if no life end game
                        restartBtn('Game-Over  '+'Your score is!! ' + score, 'Would you like to play again?', 'Restart Game');
                        clearInterval(animation);
                    } else {
                        //if have life restart
                        x = game.width/ 2;
                        y = game.height - ballRadius - paddleH;
                        dx = 2;
                        dy = -2;
                        paddleX = game.height/2;
                        paddleY = game.height - paddleH;
                        clearInterval(animation);
                        var livedown = setInterval(function(){
                            clearInterval(livedown);
                            animation = setInterval(draw,speed);
                        },1000) 
                    }
            }
            //Paddle Movement
            if (rightPressed && paddleX < game.width - paddleW) {
                paddleX += 7;

            }
            else if (leftPressed && paddleX > 0) {
                paddleX -= 7;

            }

            if (upPressed && paddleY > game.height - paddleY) {
                paddleY -= 2;

            } else if (downPressed && paddleY < game.height - paddleH) {
                paddleY += 2;

            }

            

        }
        //controllers
        document.addEventListener("keydown", keyDownHandler, false);
        document.addEventListener("keyup", keyUpHandler, false);
        function keyDownHandler(e) {
            if (e.keyCode == 68) {
                rightPressed = true;
            }
            else if (e.keyCode == 65) {
                leftPressed = true;
            }
            else if (e.keyCode == 87) {

                upPressed = true;
            }
            else if (e.keyCode == 83) {

                downPressed = true;
            }
        }

        function keyUpHandler(e) {
            if (e.keyCode == 68) {
                rightPressed = false;
            }
            else if (e.keyCode == 65) {
                leftPressed = false;
            }
            else if (e.keyCode == 87) {

                upPressed = false;
            }
            else if (e.keyCode == 83) {

                downPressed = false;
            }
        }



        function welcomeScreen() {

            //welcome main
            var wellcome = document.createElement('div');
            wellcome.classList.add('wellcomeScreen');
            brickWrapper.appendChild(wellcome);

            var wellcomeContent = document.createElement('div');
            wellcomeContent.classList.add('wellcomeContent');

            wellcome.appendChild(wellcomeContent);

            //Header
            // var wellcomeTitle = document.createElement('h1');
            // wellcomeTitle.classList.add('wellcomeTitle');
            // var wellcomeText = document.createTextNode("Squish Brick");
            // wellcomeTitle.appendChild(wellcomeText);

            // wellcomeContent.appendChild(wellcomeTitle);

            // START BUTTON
            var startBtn = document.createElement('button');
            startBtn.setAttribute('id', 'startBtn');
            startBtn.classList.add('brickBtn');
            var btnText = document.createTextNode("Start");
            startBtn.appendChild(btnText);

            wellcomeContent.appendChild(startBtn);
            //SETTINGS
            var startBtn = document.createElement('button');
            startBtn.setAttribute('id', 'settingBtn');
            startBtn.classList.add('brickBtn');
            var btnText = document.createTextNode("Controllers");
            startBtn.appendChild(btnText);
            clearInterval(animation);
            wellcomeContent.appendChild(startBtn);
        }
        welcomeScreen();

        function focusCanvas() {
            var canvas = document.getElementById('squish-brick');
            canvas.focus();
        }

        function gameIfo(title, text, buttonText) {
            var brickWrapper = document.getElementById('brickWrapper');
            var info = document.createElement('div'),
                content = document.createElement('div');
            info.classList.add('infoContent');
            content.classList.add('wellcomeContent');

            var head = document.createElement('h1'),
                body = document.createElement('p');
            var titleText = document.createTextNode(title),
                description = document.createTextNode(text),
                btnText = document.createTextNode(buttonText);

            var button = document.createElement('button');
            button.classList.add('brickBtn');
            button.classList.add('backBtn');
            button.appendChild(btnText);

            brickWrapper.appendChild(info);
            info.appendChild(content);
            content.appendChild(head);
            head.appendChild(titleText);
            content.appendChild(body);
            body.appendChild(description);
            content.appendChild(button);

            body.innerHTML = text;
            
            var back = document.getElementsByClassName('backBtn')[0];
            var contentInfo = document.getElementsByClassName('infoContent')[0];
            back.addEventListener('click', function () {
                clearInterval(animation);
                contentInfo.parentNode.removeChild(contentInfo);
                
            });

        }

        function restartBtn(title, text, buttonText) {
            var brickWrapper = document.getElementById('brickWrapper');
            var info = document.createElement('div'),
                content = document.createElement('div');
            info.classList.add('infoContent');
            content.classList.add('wellcomeContent');

            var head = document.createElement('h1'),
                body = document.createElement('p');
            var titleText = document.createTextNode(title),
                description = document.createTextNode(text),
                btnText = document.createTextNode(buttonText);

            var button = document.createElement('button');
            button.classList.add('brickBtn');
            button.classList.add('restartBtn');
            button.appendChild(btnText);

            brickWrapper.appendChild(info);
            info.appendChild(content);
            content.appendChild(head);
            head.appendChild(titleText);
            content.appendChild(body);
            body.appendChild(description);
            content.appendChild(button);

            body.innerHTML = text;
            
            var restart = document.getElementsByClassName('restartBtn')[0];
            var rContent = document.getElementsByClassName('infoContent');
            
            restart.addEventListener('click', function () {
                clearInterval(animation);
                focusCanvas();
                rContent[0].remove();
                lives = 3;
                score = 0;
                level = 1;
                speed = 12;
                fscore = 0;
                x = game.width/2;
                y = game.height - paddleH - ballRadius;
                paddleY = game.height - paddleH;
                paddleX = game.width/2;
                dx = 2;
                dy = -2;
                // Bricks Positioning
                for (let c = 0; c < brickColumnCount; c++) {
                    bricks[c] = [];
                    for (let r = 0; r < brickRowCount; r++) {
                        bricks[c][r] = { x: 0, y: 0, status: 1 };
                    }
                }
                animation = setInterval(draw, speed);
            });

        }
        
        var start = document.getElementById('startBtn'),
            settings = document.getElementById('settingBtn'),
            welcome = document.getElementsByClassName('wellcomeScreen'),
            wellcomeContent = document.getElementsByClassName('wellcomeContent');


        settings.addEventListener('click', function () {
            focusCanvas();
            clearInterval(animation);
            gameIfo('Controllers for keyboard', 'Up = W \n</br> Down = S \n</br> Right = D \n </br>Left = A\n', '<- back');
        });
        start.addEventListener('click', function () {
            focusCanvas();
            welcome[0].style.display = 'none';
            clearInterval(animation);
            //start game
            animation = setInterval(draw, speed);
        });

        clearInterval(animation);

    }
});

