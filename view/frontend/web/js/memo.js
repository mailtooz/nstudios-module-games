define([
  'jquery',

], function ($, config) {

  'use strict';

  $(document).ready(function () {
    $("body").addClass('playzone');
  });

  return function (config) {
    var cardsArray = [{
      'name': 'Blue_Penguin_PUFF',
      'img': config.options + '/Blue_Penguin_PUFF.png'
    }, {
      'name': 'Brown_Sloth_SIMON',
      'img': config.options + '/Brown_Sloth_SIMON.png'
    }, {
      'name': 'Cat_CAM',
      'img': config.options + '/Cat_CAM.png'
    }, {
      'name': 'Giraffe_GARY',
      'img': config.options + '/Giraffe_GARY.png'
    }, {
      'name': 'Green_Dino_DANNY',
      'img': config.options + '/Green_Dino_DANNY.png'
    },
    {
      'name': 'Grey_Elephant_MILA',
      'img': config.options + '/Grey_Elephant_MILA.png'
    }, {
      'name': 'Hedgehog_HANS',
      'img': config.options + '/Hedgehog_HANS.png'
    }, {
      'name': 'Purple_Unicorn_ASTRID',
      'img': config.options + '/Purple_Unicorn_ASTRID.png'
    }, {
      'name': 'Raccoon_ROCKY',
      'img': config.options + '/Raccoon_ROCKY.png'
    }, {
      'name': 'Rainbow_Unicorn_ESMERALDA',
      'img': config.options + '/Rainbow_Unicorn_ESMERALDA.png'
    }, {
      'name': 'Red_Fox_FIFI',
      'img': config.options + '/Red_Fox_FIFI.png'
    }, {
      'name': 'Teal_Alpaca_TIM',
      'img': config.options + '/Teal_Alpaca_TIM.png'
    }
    ];

    var gameGrid = cardsArray.concat(cardsArray).sort(function () {
      return 0.5 - Math.random();
    });

    var firstGuess = '';
    var secondGuess = '';
    var count = 0;
    var previousTarget = null;
    var delay = 1200;
    var win = 0;
    var game = document.getElementById('game');
    var grid = document.createElement('section');
    grid.setAttribute('class', 'memo-grid');
    game.appendChild(grid);
    var gameBtn = document.getElementById('memo-game');

    var easy = document.getElementsByClassName('btn-easy')[0];
    var hard = document.getElementsByClassName('btn-hard')[0];
    var difficult = document.getElementsByClassName('btn-difficult')[0];
    var welcome = document.getElementsByClassName('memory-main')[0];
    var countDown = document.getElementById("count-down");
    var timer;
    var score;
    var refreshIntervalId;
    var modalText = document.getElementById('modal-text');

    function start() {

      gameGrid.forEach(function (item) {
        var name = item.name,
          img = item.img;


        var card = document.createElement('div');
        card.classList.add('card');
        card.dataset.name = name;
        var front = document.createElement('div');
        front.classList.add('front');
        var back = document.createElement('div');
        back.classList.add('back');
        back.style.backgroundImage = 'url(' + img + ')';
        grid.appendChild(card);
        card.appendChild(front);
        card.appendChild(back);
      });
    }
    var match = function match() {
      var selected = document.querySelectorAll('.selected');
      selected.forEach(function (card) {
        card.classList.add('match');
      });
    };
    var resetGuesses = function resetGuesses() {
      firstGuess = '';
      secondGuess = '';
      count = 0;
      previousTarget = null;

      var selected = document.querySelectorAll('.selected');
      selected.forEach(function (card) {
        card.classList.remove('selected');
      });
    };
    function newGame() {
      var modal = document.getElementById('GameModal');
      modal.style.display = "none";

      var restart = document.querySelectorAll('.match');
      restart.forEach(function (card) {
        card.classList.remove('match');
      });
      var gameGrid = cardsArray.concat(cardsArray).sort(function () {
        return 0.5 - Math.random();
      });
      var back = document.querySelectorAll('.back');
      var card = document.querySelectorAll('.card');

      for (let i = 0; i < gameGrid.length; i++) {
        var name = gameGrid[i].name;
        var img = gameGrid[i].img;

        card[i].dataset.name = name;
        back[i].style.backgroundImage = 'url(' + img + ')';

      }
    }
    function modal() {
      var modal = document.getElementById('GameModal');
      modal.style.display = "block";
    }
    gameBtn.addEventListener('click', function (event) {
      var modal = document.getElementById('GameModal');
      goToMenu();
      modal.style.display = "none";
      countDown.innerHTML = "";
    });

    grid.addEventListener('click', function (event) {

      var clicked = event.target;

      if (clicked.nodeName === 'SECTION' || clicked === previousTarget || clicked.parentNode.classList.contains('selected') || clicked.parentNode.classList.contains('match')) {
        return;
      }

      if (count < 2) {
        count++;
        if (count === 1) {
          firstGuess = clicked.parentNode.dataset.name;
          // console.log(firstGuess);
          clicked.parentNode.classList.add('selected');
        } else {

          secondGuess = clicked.parentNode.dataset.name;
          // console.log(secondGuess);
          clicked.parentNode.classList.add('selected');
        }

        if (firstGuess && secondGuess) {
          if (firstGuess === secondGuess) {
            winGame();
            setTimeout(match, delay);

          }
          setTimeout(resetGuesses, delay);
        }
        previousTarget = clicked;
      }
    });


    function winGame() {
      win++
      if (win >= 12) {
        count = 0;
        win = 0;
        clearInterval(refreshIntervalId);
        modal();
        modalText.textContent = "You Win";

      }
    }


    function startTimer(duration, display) {
      var timer = duration, minutes, seconds;
      refreshIntervalId = setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.innerHTML = minutes + ":" + seconds;

        score = minutes + ":" + seconds;
        --timer;
        if (timer < 0) {
          clearInterval(refreshIntervalId);
          modal();
          modalText.textContent = "Times up, Try again!";
        }
      }, 1000);
    }

    function goToMenu() {
      welcome.style.display = "block";
    }

    //Game difficulty Level and start game
    easy.addEventListener('click', function () {
      timer = 180;
      startTimer(timer, countDown);
      welcome.style.display = "none";
      newGame();

    });
    hard.addEventListener('click', function () {
      timer = 150;
      startTimer(timer, countDown);
      welcome.style.display = "none";
      newGame();
    });
    difficult.addEventListener('click', function () {
      timer = 120;
      startTimer(timer, countDown);
      welcome.style.display = "none";
      newGame();
    });
    start();
  }

});

